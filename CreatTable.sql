CREATE TABLE TWEET (
	Tweet_ID		INT NOT NULL,
	Autor			VARCHAR(50) NOT NULL,
	Favoriten		INT,
	ReTweets		INT,
	Zeit			DATETIME NOT NULL,
	Inhalt			VARCHAR(256) NOT NULL,
PRIMARY KEY (Tweet_ID)
	)
	
CREATE TABLE HASHTAG (
	Name			VARCHAR(50) NOT NULL,
	Anzahl_Gesamt	INT NOT NULL,
PRIMARY KEY (Name)
	)
	
CREATE TABLE HASHTAGPAAR (
	Tupel			VARCHAR(100) NOT NULL,
	Anzahl_Gesamt	INT NOT NULL
PRIMARY KEY (Tupel)
	)

CREATE TABLE ENTHÄLT (
	Tweet_ID		INT NOT NULL,
	Hashtag_Name	VARCHAR(50) NOT NULL,
PRIMARY KEY (Tweet_ID, Hashtag_Name),
FOREIGN KEY (Tweet_ID) REFERENCES TWEET.Tweet_ID,
FOREIGN KEY (Hashtag_Name) REFERENCES HASHTAG.Name
	)
	
CREATE TABLE TUPEL_AUS (
	Hashtag_Name	VARCHAR(50) NOT NULL,
	Hashtag_Tupel 	VARCHAR(100) NOT NULL,
PRIMARY KEY (Hashtag_Name, Hashtag_Tupel),
FOREIGN KEY (Hashtag_Name) REFERENCES HASHTAG.Name,
FOREIGN KEY (Hashtag_Tupel) REFERENCES HASHTAGPAAR.Tupel
	)
