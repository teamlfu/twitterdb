<!-- DBS 2017 -->
<!-- Gruppe 4 -->
<!-- Alena Dudarenok -->
<?php 
  $Hashtag = $_POST["Hashtag"];
//  echo $Hashtag;
//  echo "<br />\n";
  $counts = array();
  $dates = array();
  $datesCounts = array();
  $datesCountsSpecial = array();
  $countsSpecial = array();
  $datesSpecial = array();
  $hashcount = array();

  $array = file_get_contents("american-election-tweets_bereinigt.csv");
  $array2 = explode(';',$array);
  $i = sizeof($array2);
  $sem = 0;
  $set = 1;
//  echo "Set: $set <br/>\n";
  $strout = "";
  for($n = 0;$n < $i;$n++,$sem++)
  {
    if($sem == 4)
    {
      $array3 = explode(PHP_EOL,$array2[$n]);
/*      echo $array3[0] ."<br/><br/>\n";
      if($n < $i - 1)
      {
        echo "Set: $set <br/>\n";
        echo $array3[1] ."<br/>\n";
      }
 */     $sem = 0;
      $set++;
    }
    else
    if($sem == 2 && $set != 1)
    {
      $regex = '#^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$#';
      $da = $array2[$n];
      if (preg_match($regex, $da)) 
      {
//        echo "$strout <br/>\n";

        preg_match_all('/#([^\s]+)/', $strout, $matches);
        $hashtags = implode(',', $matches[1]);

        $sma = sizeof($matches[1]);
        $rest = substr($array2[$n], 0, -9); 
        if(array_key_exists($rest, $datesCountsSpecial) == false)
        {
          $datesCountsSpecial[$rest] = 0;
        }
        for($j = 0;$j < $sma;$j++)
        {
          if(array_key_exists($matches[1][$j], $hashcount) == true)
          {
            $hashcount[$matches[1][$j]]++;
          }
          else
          {
            $hashcount[$matches[1][$j]] = 1;
          }
          if($matches[1][$j] == $Hashtag)
          {
              $datesCountsSpecial[$rest]++;
          }
        }
//        echo "Hashtags ($sma): $hashtags <br/>\n";

  //      echo "$rest <br/>\n";

        if(array_key_exists($rest, $datesCounts) == true)
        {
          $datesCounts[$rest] += $sma;
        }
        else
        {
          $datesCounts[$rest] = $sma;
        }
      } 
      else 
      {
        $strout .= ';';
        $strout .= $array2[$n];
        $sem--;
      }      
    }
    else
    if($sem == 1 && $set != 1)
      $strout = $array2[$n];
//    else
//      echo "$array2[$n] <br/>\n";
  }
  ksort($datesCounts);
  $counts = array_values($datesCounts);
  $dates = array_keys($datesCounts);
  arsort($hashcount);
  ksort($datesCountsSpecial);
  $countsSpecial = array_values($datesCountsSpecial);
  $datesSpecial = array_keys($datesCountsSpecial);
?>
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8"></meta>
  <title>Datenvisualisierung</title>
  <link href="my.css" rel="stylesheet" type="text/css">
  <script language="javascript" type="text/javascript" src="./libs/jquery.js"></script>
  <script language="javascript" type="text/javascript" src="./libs/jquery.flot.js"></script>
  <script language="javascript" type="text/javascript" src="./libs/jquery.flot.categories.js"></script>
  <script type="text/javascript">
  $(function() {

    
    var data = [];
    /* = [["02.01.17",2330417],["03.01.17",882614],["04.01.17",449615],["05.01.17",746100],
                ["06.01.17",700163],["09.01.17",1297576],["10.01.17",1691504],["11.01.17",1521071],
                ["12.01.17",1073743],["13.01.17",1149538],["16.01.17",861942],["17.01.17",848050],
                ["18.01.17",528968],["19.01.17",778900],["20.01.17",610841],["23.01.17",366744],
                ["24.01.17",734456],["25.01.17",1221101],["26.01.17",1302214],["27.01.17",739897],
                ["30.01.17",1268146],["31.01.17",988174]];
*/
      var y = $.parseJSON('<?php echo json_encode($counts); ?>');
      var d = $.parseJSON('<?php echo json_encode($dates); ?>');
      $i = 0;
      for(i = 0;i < y.length;i++)
      {
        var t = [];
        t.push(d[i]);
        t.push(y[i]);
        data.push(t);//daten;
//        console.log(t[0] + " , " + t[1]);
      }
    $.plot("#barchart", [ data ], {
      series: {
        bars: {
          show: true,
          barWidth: 0.1,
          align: "center",
        }
      },
      xaxis: {
          show: false,
        mode: "categories",
        tickLength: 0
//        ticks: 10
      },
      colors: ["#FF0000"]
    });    

    var dataS = [];
      var yS = $.parseJSON('<?php echo json_encode($countsSpecial); ?>');
      var dS = $.parseJSON('<?php echo json_encode($datesSpecial); ?>');
      $i = 0;
      for(i = 0;i < yS.length;i++)
      {
        var t = [];
        t.push(dS[i]);
        t.push(yS[i]);
        dataS.push(t);//daten;
//        console.log(t[0] + " , " + t[1]);
      }
    $.plot("#barchartSpecial", [ dataS ], {
      series: {
        bars: {
          show: true,
          barWidth: 0.1,
          align: "center"
        }
      },
      xaxis: {
          show: false,
        mode: "categories",
        tickLength: 0
//        ticks: 10
      },
      colors: ["#00FF00"]
    });    

  });
  </script>
  </head>
  <body>
    <div style="text-align:center">
      <h2> <?php echo "Datenvisualisierung"; ?> </h2>
    </div>
    <div id="content">
      <div class="demo-container">
      <h3> <?php echo "Gesamtanzahl der Hashtags (2016-01-05 bis 2016-09-28)" ?> </h3>
        <div id="barchart" class="demo-placeholder"></div>
      </div>
    </div>
    <div id="content">
      <div class="demo-container">
      <h3> <?php echo "Hashtag: $Hashtag (2016-01-05 bis 2016-09-28)"; ?> </h3>
        <div id="barchartSpecial" class="demo-placeholder"></div>
      </div>
    </div>
    <form method="post">
      <div style="text-align:center">
        <label>
         <p>Hashtags: (sortiert nach der Gesamtanzahl)</p>
          <select name="Hashtag" size="5" onchange="this.form.submit()">
            <?php
              $hash = array_keys($hashcount);
              $j = sizeof($hash);
              for($i = 0;$i < $j;$i++)
              {
                echo "<option value='$hash[$i]'>$hash[$i]</option>";
              }
            ?>
          </select>
        </label>
      </div>
    </form>
  </body>
</html>

