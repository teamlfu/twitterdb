def k_means(x,y,z):
	filename = './Hashtags.csv'
	election = csv.DictReader (open(filename), delimiter = ';' , fieldnames = ['a'])
	listx = []
	listy = []
	listz = []
	xa= 0
	ya= 0
	za= 0
	for row in election:
		ascii = sum([ord(c) for c in row['a']])
		a=abs(x-ascii)
		b=abs(y-ascii)
		c=abs(z-ascii)
		d=min(a,b,c)
		if d==a:
			listx.append(row['a'])
			xa = xa + ascii
		elif d==b:
			listy.append(row['a'])
			ya = ya + ascii
		else:
			listz.append(row['a'])
			za = za + ascii
	xa = xa/max(len(listx),1)
	ya = ya/max(len(listy),1)
	za = za/max(len(listz),1)
	if abs(xa-x) > 1 and abs(ya-y) > 1 and abs(za-z) > 1:
		k_means(xa,ya,za)
	else:
		print (x, listx)
		print (y, listy)
		print (z, listz)
	return
