# -*- coding: utf-8 -*-
import itertools
import json
import csv
import re

#Ziel ist ein dictionary welches zu json konvertiert wird
#um direkt nach sigma zu parsen
#enthält die Kanten und die Knoten
knotenkantendict = {"nodes": [], "edges":[]}

#liste aller tweets
tweetlist= []

#sets in python enthalten jedes Element nur einmal
#alle Hashtags einmal:
allhashtagset = set()
#aus jedem listen Elemt von tweet_list ein Tupel bauen
hashtagtupleset = set()

#mit csv.DictReader datei einlesen
filename = 'american-election-tweets_bereinigt.csv'
#encoding soll utf8 sein und falls es Fehler gibt werden sie übergangen
with open(filename, 'rt', encoding="utf8", errors='replace') as csvfile:
	csvdata = csv.reader(csvfile, delimiter=';')
#nur die Spalte text wird gelesen da hier die tweets stehen
	for row in csvdata:
		tweettext = row[1]
		tweetlist.append(tweettext)

hashtaglist = []
for i in tweetlist:
	#Liste aller Hashtags innerhalb eines tweets
	hashtaglist.append(re.findall(r"#(\w+)", i))

#Damit keine Doppelung bei den Kanten vorkommt
hashtaglist.sort()

#hashtagtupelset mit allen tupeln der möglichen Kanten füllen

for j in range(0, len(hashtaglist)):
	if len(hashtaglist[j]) == 2:
		hashtagtupleset.add((hashtaglist[j][0], hashtaglist[j][1]))
	elif len(hashtaglist[j]) > 2:
		for k in range(0,len(hashtaglist[j]) - 1):
			for l in range(k + 1, len(hashtaglist[j])):
				hashtagtupleset.add((hashtaglist[j][k], hashtaglist[j][l]))

#alle Hashtags in das set hashtaglist einspeisen

for u in range(0, len(hashtaglist)):
	for tag in hashtaglist[u]:
		allhashtagset.add(tag)

#schreiben der "nodes" für die knotenkantendict
#mit der richtigen Reihenfolge und Formatierung kann dann nachher
#die json datei direkt als sigma graph mit den enstprechenden
#Knoten und Kanten geplottet werden
idcountknoten = 1

for n in allhashtagset:
	knotendict = {} #immer leer zu beginn der neuen Iteration
	knotendict['id'] = n #Konten id
	knotendict['label'] = n #der entsprechende Hashtag
	#die Koordinaten werden in der .html Datei erstellt
	knotendict['x'] = idcountknoten
	knotendict['y'] = idcountknoten
	knotendict['size'] = 1
	knotendict['color'] = '#f00'
	idcountknoten += 1
	#einfügen in knotenkantendict
	knotenkantendict["nodes"].append(knotendict)

idcountkanten = 1

#gleiches Vorgehen wie für Knoten 
for e in hashtagtupleset:
	kantendict = {}
	kantendict['id'] = str(idcountkanten) #id festlegen
	kantendict['source'] = e[1] #zweiter TeilTupel wird Quelle der Kante
	kantendict['target'] = e[0] #erster TeilTupel wird Ziel der Kante
	idcountkanten += 1
	knotenkantendict["edges"].append(kantendict)
print(knotenkantendict)

#neue .json datei erstellen und das dictionary rein parsen
with open('hashtagsdict.json', 'w') as fp:
    json.dump(knotenkantendict, fp)
