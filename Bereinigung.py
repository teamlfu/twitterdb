import csv
def clean():
	filename = 'C:\\Users\\ScaredFish/downloads/american-election-tweets.csv'
	election = csv.DictReader (open(filename)) #Hier wird die Datei in einer für Python leicht bearbeitbaren Form aufgerufen
	electiontweets = csv.writer(open(filename[:-4] + "_bereinigt.csv" ,'w', newline=''), delimiter=';')
	#Hier wird die neue Datei erstellt, wobei ein Semikolon als Trennzeichen benutzt wird.
	for row in election:
		if (len(row['handle']) > 0 and len(row['text']) > 0 and len(row['time']) > 0 and len(row['retweet_count']) > 0 and
		len(row['favorite_count']) > 0):
			electiontweets.writerow([row['handle']] + [row['text']] + [row['time']] + [row['retweet_count']] + [row['favorite_count']])
#Hier werden nur diejenigen Reihen ausgegeben, bei denen in den relevanten Spalten Einträge sind (len>0).
	return