# -*- coding: utf-8 -*-
import psycopg2
import csv

csv_data = csv.reader(file('american-election-tweets.csv'))
#read csv Datei
#importieren von psycopg2 um zugriff auf die DB zu erlangen

dbstring = ("dbname='Election' user='postgres' host='localhost'" + \
"password='linux'")
#Datenbank string DB Election, als Benutzer "postgres", über localhost mit
#entprechendem PW

try:
	database = psycopg2.connect(dbstring)

except Exception as e:
#Excetion die geworfen wird wenn etwas schief geht
	print("Can't connect.")
	print(e)

SQL_STATEMENT_DROP = """DROP TABLE IF EXISTS tweets"""
#SQL Befehl um auf alle Zeilen der CSV Datei zuzugreifen

cursor = database.cursor()
cursor.execute(SQL_STATEMENT_DROP)

SQL_STATEMENT_CREATETABLE = "CREATE TABLE tweets (handle varchar(30),"\
"text varchar(144),is_retweet BOOLEAN,original_author varchar(30),"\
"time DATETIME NOT NULL, in_reply_to_screen_name varchar(30),"\
"is_quote_status BOOLEAN, retweet_count INTEGER NOT NULL,"\
"favorite_count INTEGER NOT NULL, source_url varchar(256), truncated BOOLEAN);"

cursor.execute(SQL_STATEMENT_CREATETABLE)

SQL_STATEMENT_INSERT = "INSERT INTO tweets (handle, text, is_retweet"\
", original_author, time, in_reply_to_screen_name, is_quote_status"\
", retweet_count, favorite_count, source_url, truncated) VALUES"\
" (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"

for row in csv_data:
	cursor.execute(SQL_STATEMENT_INSERT, row)

cursor.close()
database.commit()
database.close()

print("CSV erfolgreich importiert")
